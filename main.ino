// DHT Temperature & Humidity Sensor
// Unified Sensor Library Example
// Written by Tony DiCola for Adafruit Industries
// Released under an MIT license.

// Depends on the following Arduino libraries:
// - Adafruit Unified Sensor Library: https://github.com/adafruit/Adafruit_Sensor
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <LiquidCrystal_I2C.h>

#define DHTPIN            2         // Pin which is connected to the DHT sensor.
#define RELAY 3
#define DHTTYPE           DHT11     // DHT 11 
const int tempPin = A0;  // Analog input pin that the potentiometer is attached to
const int humPin = A1; 

int sensorTempValue = 0;        // value read from the pot
int temp_treshhold = 0;        // value output to the PWM (analog out)

int sensorHumidityValue = 0;        // value read from the pot
int humidity_treshhold = 0;
int counter = 0;
bool canDisableFan = true;

DHT_Unified dht(DHTPIN, DHTTYPE);
LiquidCrystal_I2C lcd(0x27, 16, 2);

uint32_t delayMS;
bool tempOn;
bool humOn;
bool lcdOn = false;


void setup() {
  Serial.begin(115200);
  lcd.init(); 
  lcd.backlight();
  dht.begin();
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  dht.humidity().getSensor(&sensor);
  delayMS = sensor.min_delay / 1000;
  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, HIGH);
}

void loop() {
  lcd.setCursor(0,0);
  lcd.print("Tem:");
  lcd.setCursor(7,0);
  lcd.print("Wil:");
  lcd.setCursor(0,1);
  lcd.print("Zad:");
  lcd.setCursor(7,1);
  lcd.print("Zad:");
  lcd.setCursor(14,0);
  lcd.print("W:");
  Serial.print("relay:");
  Serial.println(digitalRead(RELAY));

  sensorTempValue = analogRead(tempPin);
  temp_treshhold = map(sensorTempValue, 0, 1023, 15, 40);
  
  lcd.setCursor(4,1);
  lcd.print(temp_treshhold);

  sensorHumidityValue = analogRead(humPin);
  humidity_treshhold = map(sensorHumidityValue, 0, 1023, 30, 100);

  Serial.print("zadana temp:");
  Serial.println(temp_treshhold);
  Serial.print("zadana wilgotnosc:");
  Serial.println(humidity_treshhold);

  lcd.setCursor(11,1);
  lcd.print(humidity_treshhold);

  delay(delayMS);
  // Get temperature event and print its value.
  sensors_event_t event;  
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println("Error reading temperature!");
    lcd.setCursor(4,0);
    lcd.print("Er");  
  }
  else {
    if(temp_treshhold <= event.temperature){
         Serial.println("temp: true");
      tempOn = true;
    } else {
      Serial.println("temp: false");
      tempOn = false;
    }
    Serial.print("Temperature: ");
    Serial.print(event.temperature);
    lcd.setCursor(4,0);
    lcd.print((int)event.temperature);
    Serial.println(" *C");
  }
  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println("Error reading humidity!");
    lcd.setCursor(11,0);
    lcd.print("Er");
  }
  else { 
    if(humidity_treshhold <= event.relative_humidity){
      Serial.println("hum: true");
      humOn = true;
    } else {
      Serial.println("hum: false");
      humOn = false;
    }
  
    Serial.print("Humidity: ");
    Serial.print(event.relative_humidity);
    lcd.setCursor(11,0);
    lcd.print((int)event.relative_humidity);
    Serial.println("%");
  }

  if(tempOn && humOn) {
    Serial.println("relay on");
    digitalWrite(RELAY, LOW);
    lcd.setCursor(14,1);
    lcd.print(" T");
    counter = 0;
    canDisableFan = false;
  }
  else {
    Serial.print("canDisableFan ");
    Serial.println(canDisableFan);
    if(canDisableFan){
      Serial.println("relay off");
      digitalWrite(RELAY, HIGH);
      lcd.setCursor(14,1);
      lcd.print(" N");
    } else{
      lcd.setCursor(14,1);
      lcd.print("  "); //clear line
      lcd.setCursor(14,1);
      lcd.print(counter);
    }
  }
  if(counter++ >= 90){
    canDisableFan = true;
  }
}